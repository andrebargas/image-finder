from flask_seeder import Seeder
from debian_image_finder.models import Image
from debian_image_finder import db
import json


class ImageSeeder(Seeder):

    def run(self):
        json_file = open('seed.json')
        data = json.load(json_file)

        for image in data['images']:
            print('arch: %s' % image['arch'])
            print('release: %s' % image['release'])
            print('img_type: %s' % image['img_type'])
            print('vendor: %s' % image['vendor'])
            print('version: %s' % image['version'])
            print('region: %s' % image['region'])
            print('ref: %s' % image['ref'])
            print('packages: %s' % image['packages'])
            print('created_at: %s' % image['created_at'])
            print('provider_id: %s' % image['provider_id'])
            print('')
            db.session.add(Image(arch=image['arch'],
                                 release=image['release'],
                                 img_type=image['img_type'],
                                 vendor=image['vendor'],
                                 version=image['version'],
                                 region=image['region'],
                                 ref=image['ref'],
                                 packages=image['packages'],
                                 created_at=image['created_at'],
                                 provider_id=image['provider_id']))
