from flask_seeder import Seeder
from debian_image_finder.models import Provider
from debian_image_finder import db
import json


class ProviderSeeder(Seeder):

    def run(self):
        json_file = open('seed.json')
        data = json.load(json_file)

        for provider in data['providers']:
            print('Name: ' + provider['name'])
            print('Description: ' + provider['description'])
            print('Markdown File: ' + provider['markdown_file'])
            print('')
            db.session.add(Provider(name=provider['name'],
                                    description=provider['description'],
                                    markdown_file=provider['markdown_file']))
