from typing import List, Set

from bs4.element import ResultSet
import requests
from bs4 import BeautifulSoup

import re
from debian_image_finder.models import Image, Provider
from debian_image_finder import db
from datetime import datetime
import logging

IMAGE_ANCHOR_TEXT_REGEX = re.compile('\d{8}-\d{3}/')


def load_only_new_images(site_url: str, image_type: str) -> None:
    """
    Fetch data and insert only new releases images versions data in DB.
    """
    release_anchors = _get_site_repository_anchors_filter_by_regex(site_url)
    release_anchors_texts = set([anchor.text for anchor in release_anchors])
    logging.info(f"{len(release_anchors_texts)} images found on {site_url}")

    repository_images_versions = _get_release_images_versions(image_type=image_type)
    absent_images = release_anchors_texts - repository_images_versions
    logging.info(f"{len(absent_images)} images are absent in DB")

    for image_version in absent_images:
        image_url = site_url + image_version
        logging.info(f"Getting data from image version {image_version}")
        _add_new_images_from_version(image_url)
        logging.info(f"Image version {image_version} added to DB")
    db.session.commit()


def _add_new_images_from_version(image_url: str) -> None:
    """
    From a image version page this function creates multiples Images in to DB, one for
    each distribution, architecture and provider.
    """
    response = requests.get(image_url)
    content = response.content
    site = BeautifulSoup(content, 'html.parser')

    for json_file_row in site.find_all('a', text=re.compile('.json$')):
        image_json = requests.get(image_url + json_file_row.text).json()
        new_image = _create_new_image(image_json)
        db.session.add(new_image)


def _get_release_images_versions(image_type: str) -> Set[str]:
    """
    Returns all the different images versions present in DB.
    """
    release_images = Image.query.filter(Image.img_type == image_type)
    return set([image.version for image in list(release_images)])


def _get_site_repository_anchors_filter_by_regex(site_url: str) -> ResultSet:
    """
    Fetch anchor HTML data from regex expression. The anchors will be fetch from the
    anchor text data by the regex expression.
    """
    response = requests.get(site_url)
    content = response.content
    site = BeautifulSoup(content, 'html.parser')
    return site.find_all('a', text=IMAGE_ANCHOR_TEXT_REGEX)


def _add_new_provider(provider_name: str) -> None:
    """
    Add a new cloud provider in DB
    """
    new_provider = Provider(provider_name, "", "")
    db.session.add(new_provider)
    logging.info(f"{provider_name} added to DB")
    db.session.commit()


def _get_provider_id(provider_name: str) -> int:
    """
    Get the provider ID querying by the provider name. It new always be only one Id per
    name due the name attribute be unique in db scheme.
    """
    query_result = Provider.query.filter(Provider.name == provider_name)
    if len(list(query_result)) == 0:
        logging.warning(f"Provider {provider_name} not found !!!")
        _add_new_provider(provider_name)
        _get_provider_id(provider_name)

    return query_result[0].id


def _parse_packages_data(packages: List[dict[str, str]]) -> str:
    """
    Parse packages json data from dict list to string format. The sting format is
    "package name : package vesion".
    """
    packages_str = ''
    for package in packages:
        package_str = f"{package['name']}: {package['version']}, "
        packages_str += package_str
    return packages_str


def _create_new_image(image_json: dict) -> Image:
    """
    Create a new Image object with the json meta data fetched from the Debian images
    repository.
    """
    meta_data = image_json['items'][1]['metadata']
    data = image_json['items'][1]['data']

    image_provider_map = {
        "azure": "Microsoft Azure",
        "ec2": "Amazon Web Services (AWS)",
        "gce": "Google Cloud",
        "generic": "Generic",
        "genericcloud": "Generic Cloud",
        "nocloud": "NoCloud"
    }

    arch = meta_data['labels']['debian.org/arch']
    release = meta_data['labels']['debian.org/release']
    img_type = meta_data['labels']['upload.cloud.debian.org/type']
    vendor = meta_data['labels']['cloud.debian.org/vendor']
    version = meta_data['labels']['cloud.debian.org/version']
    region = 'all'
    ref = data['ref']
    packages = _parse_packages_data(image_json['items'][0]['data']['packages'])
    created_at = datetime.utcnow()
    provider_id = _get_provider_id(image_provider_map[vendor])

    return Image(arch, release, img_type, vendor, version, region, ref, packages,
                 created_at, provider_id)
