from debian_image_finder.jobs.shared import load_only_new_images
import logging

URL = "https://cdimage.debian.org/cdimage/cloud/bullseye/daily/"


def full_load_only_new_daily_images() -> None:
    logging.info(f"Starting full load daily images from url {URL}")
    load_only_new_images(image_type="daily", site_url=URL)


full_load_only_new_daily_images()
