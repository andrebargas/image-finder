from debian_image_finder.jobs.shared import load_only_new_images
import logging

URL = "https://cdimage.debian.org/cdimage/cloud/bullseye/"


def load_only_new_release_images() -> None:
    logging.info(f"Starting full load release images from url {URL}")
    load_only_new_images(image_type="release", site_url=URL)
