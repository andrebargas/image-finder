import json
import unittest

from debian_image_finder.tests.base import BeforeTestUserCommon
from debian_image_finder.models import Provider
from debian_image_finder import db


class TestProviderService(BeforeTestUserCommon):
    """Tests for the Provider Service."""

    def test_post_provider(self):
        args = {
            "name": "provider",
            "description": "test",
            "markdown_file": "file"
        }
        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.post("/api/provider",
                                    data=json.dumps(args),
                                    headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider', data['name'])
        self.assertIn('test', data['description'])
        self.assertIn('file', data['markdown_file'])

    def test_get_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        response = self.client.get('/api/provider/1')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider', data['name'])
        self.assertIn('test', data['description'])
        self.assertIn('file', data['markdown_file'])

    def test_put_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        args = {
            "name": "provider_change",
            "description": "test_change",
            "markdown_file": "file_change"
        }
        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.put("/api/provider/1",
                                   data=json.dumps(args),
                                   headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider_change', data['name'])
        self.assertIn('test_change', data['description'])
        self.assertIn('file_change', data['markdown_file'])

    def test_delete_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.delete('/api/provider/1',
                                      headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider', data['name'])
        self.assertIn('test', data['description'])
        self.assertIn('file', data['markdown_file'])


if __name__ == '__main__':
    unittest.main()
