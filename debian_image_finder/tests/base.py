from flask_testing import TestCase
from debian_image_finder import app, db
from werkzeug.security import generate_password_hash
from debian_image_finder.models import User
import uuid
import datetime
import jwt


class BaseTestCase(TestCase):
    def create_app(self):
        app.config.from_object('config.TestingConfig')
        return app

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class BeforeTestUserCommon(BaseTestCase):
    hashed_password = generate_password_hash('123456', method='sha256')

    admin_user = User(public_id=str(uuid.uuid4()),
                      name='admin',
                      password=hashed_password,
                      admin=True)

    db.session.add(admin_user)
    db.session.commit()

    exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
    token = jwt.encode({'public_id': admin_user.public_id, 'exp': exp},
                       app.config['SECRET_KEY'])
