from debian_image_finder import db
from debian_image_finder import ma
import os


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50))
    password = db.Column(db.String(80))
    admin = db.Column(db.Boolean)

    def __init__(self, public_id, name, password, admin):
        self.public_id = public_id
        self.name = name
        self.password = password
        self.admin = admin

    def __repr__(self):
        return '<User %r>' % self.name


class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id',
                  'public_id',
                  'name',
                  'admin')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class Provider(db.Model):
    __tablename__ = "providers"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.Text)
    markdown_file = db.Column(db.String(100))

    def __init__(self, name, description, markdown_file):
        self.name = name
        self.description = description
        self.markdown_file = markdown_file

    def __repr__(self):
        return '<Provider %r>' % self.name


class ProviderSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id',
                  'name',
                  'description',
                  'markdown_file')


provider_schema = ProviderSchema()
providers_schema = ProviderSchema(many=True)


class Image(db.Model):
    __tablename__ = "images"
    id = db.Column(db.Integer, primary_key=True)
    arch = db.Column(db.String, nullable=False)
    release = db.Column(db.String, nullable=False)
    img_type = db.Column(db.String, nullable=False)
    vendor = db.Column(db.String, nullable=False)
    version = db.Column(db.String, nullable=False)
    region = db.Column(db.String)
    artifact_url = db.Column(db.String)
    ref = db.Column(db.String, unique=True)
    packages = db.Column(db.Text)
    created_at = db.Column(db.DateTime, nullable=False)
    provider_id = db.Column(db.Integer, db.ForeignKey('providers.id'))

    provider = db.relationship('Provider', foreign_keys=provider_id)

    def __init__(self,
                 arch,
                 release,
                 img_type,
                 vendor,
                 version,
                 region,
                 ref,
                 packages,
                 created_at,
                 provider_id):
        self.arch = arch
        self.release = release
        self.img_type = img_type
        self.vendor = vendor
        self.version = version
        self.region = region
        self.ref = self.refactor_ref_param(ref)
        self.artifact_url = self.build_artifact_url(ref, region)
        self.packages = packages
        self.created_at = created_at
        self.provider_id = provider_id

    def build_artifact_url(self, artifact_url, region=None):
        if artifact_url.startswith('ami'):
            base_url = os.environ.get("BASE_AWS_ARTIFACT_URL", "")
            url = f'{base_url}?region={region}#launchAmi={artifact_url}'
        else:
            base_url = os.environ.get("BASE_ARTIFACT_URL", "")
            url = f'{base_url}{artifact_url}'
        return url

    def refactor_ref_param(self, ref):
        if not ref.startswith('ami'):
            without_last_slash = ref.split('/')[-1]
            # This removes .tar.xz at the end of the string
            without_last_seven_chars = without_last_slash[0:-7]
            return without_last_seven_chars
        else:
            return ref

    def __repr__(self):
        return '<Image %r>' % self.release


class ImageSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id',
                  'arch',
                  'release',
                  'img_type',
                  'vendor',
                  'version',
                  'region',
                  'artifact_url',
                  'ref',
                  'packages',
                  'created_at',
                  'provider_id')


image_schema = ImageSchema()
images_schema = ImageSchema(many=True)
