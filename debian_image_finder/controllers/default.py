import markdown2
import requests
from flask import render_template, make_response, request, url_for
from debian_image_finder import app
from debian_image_finder.models import Provider
from debian_image_finder.models import Image
from config import RESULTS_PER_PAGE
from debian_image_finder.forms import SearchForm


@app.route("/")
def index():
    all_providers = Provider.query.all()
    return render_template('home.html',
                           Providers=all_providers)


@app.route("/provider/<id>", methods=['GET', 'POST'])
def provider(id):
    form = SearchForm()
    provider = Provider.query.get(id)
    markdown = 'Could not fetch description.'

    try:
        response = requests.get(provider.markdown_file)
        if response.status_code == 200:
            markdown = response.text
    except Exception:
        app.logger.error(ConnectionError)

    releases = Image.query.filter_by(provider_id=id)\
                          .with_entities(Image.release)\
                          .order_by(Image.release).distinct()
    for release in releases:
        form.release.choices.append((release[0], release[0]))

    img_types = Image.query.filter_by(provider_id=id)\
                           .with_entities(Image.img_type)\
                           .order_by(Image.img_type).distinct()
    for img_type in img_types:
        form.img_type.choices.append((img_type[0], img_type[0]))

    archs = Image.query.filter_by(provider_id=id)\
                       .with_entities(Image.arch)\
                       .order_by(Image.arch).distinct()
    for arch in archs:
        form.arch.choices.append((arch[0], arch[0]))

    regions = Image.query.filter_by(provider_id=id)\
                         .with_entities(Image.region)\
                         .order_by(Image.region).distinct()
    for region in regions:
        form.region.choices.append((region[0], region[0]))

    content = markdown2.markdown(markdown)
    page = request.args.get('page', default=1, type=int)
    query = Image.query.filter_by(provider_id=id)
    total_count = query.count()

    ref = request.args.get('ref', default='', type=str)
    version = request.args.get('version', default='', type=str)
    release = request.args.get('release', default='', type=str)
    img_type = request.args.get('img_type', default='release', type=str)
    arch = request.args.get('arch', default='', type=str)
    region = request.args.get('region', default='', type=str)

    if form.validate_on_submit():
        ref = form.ref.data
        version = form.version.data
        release = form.release.data
        img_type = form.img_type.data
        arch = form.arch.data
        region = form.region.data

    if ref:
        form.ref.data = ref
        query = query.filter_by(ref=ref)
    if version:
        form.version.data = version
        query = query.filter_by(version=version)
    if release:
        form.release.data = release
        query = query.filter_by(release=release)
    if img_type:
        form.img_type.data = img_type
        query = query.filter_by(img_type=img_type)
    if arch:
        form.arch.data = arch
        query = query.filter_by(arch=arch)
    if region:
        form.region.data = region
        query = query.filter_by(region=region)

    query = query.order_by(Image.created_at.desc())

    filtered_count = query.count()
    images = query.paginate(page=page,
                            per_page=RESULTS_PER_PAGE,
                            error_out=False)

    def url_for_page(page):
        return url_for('provider',
                       id=provider.id,
                       page=page,
                       ref=ref,
                       version=version,
                       release=release,
                       img_type=img_type,
                       arch=arch,
                       region=region)

    return render_template('provider.html',
                           provider=provider,
                           images=images,
                           content=content,
                           form=form,
                           url_for_page=url_for_page,
                           total_count=total_count,
                           filtered_count=filtered_count)


@app.route("/image/<ref>")
def image(ref):
    image = Image.query.filter_by(ref=ref).first_or_404()
    packages = markdown2.markdown(image.packages)
    return render_template('image.html',
                           image=image,
                           provider=image.provider,
                           packages=packages)


@app.route('/provider/<id>/feed')
def feed(id):
    all_images_from_provider = Image.query.filter_by(provider_id=id).all()
    template = render_template('feed.xml',
                               Images=all_images_from_provider,
                               baseurl=request.url_root)
    response = make_response(template)
    response.headers['Content-Type'] = 'application/xml'
    return response
