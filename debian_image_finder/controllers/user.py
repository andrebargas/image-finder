from flask import request, jsonify, make_response
from debian_image_finder import app
from debian_image_finder import db
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
from functools import wraps
from debian_image_finder.models import User, user_schema, users_schema


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            user = User.query.filter_by(public_id=data['public_id']).first()
        except Exception:
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(user, *args, **kwargs)

    return decorated


@app.route('/api/user', methods=['GET'])
@token_required
def get_all_users(user):

    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    users = User.query.all()

    output = users_schema.dump(users)
    return jsonify({'users': output})


@app.route('/api/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(user, public_id):

    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})

    return user_schema.jsonify(user)


@app.route('/api/user', methods=['POST'])
@token_required
def create_user(user):
    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    data = request.get_json()

    hashed_password = generate_password_hash(data['password'], method='sha256')

    admin = False if 'admin' not in data else data['admin']

    new_user = User(public_id=str(uuid.uuid4()),
                    name=data['name'],
                    password=hashed_password,
                    admin=admin)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message': 'New user created!'})


@app.route('/api/user/promote/<public_id>', methods=['PUT'])
@token_required
def promote_user(user, public_id):
    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})

    user.admin = True
    db.session.commit()

    return jsonify({'message': 'The user has been promoted!'})


@app.route('/api/user/unpromote/<public_id>', methods=['PUT'])
@token_required
def unpromote_user(user, public_id):
    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})

    user.admin = False
    db.session.commit()

    return jsonify({'message': 'The user has been unpromoted!'})


@app.route('/api/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(user, public_id):
    if not user.admin:
        return jsonify({'message': 'Cannot perform that function!'})

    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'message': 'No user found!'})

    db.session.delete(user)
    db.session.commit()

    return jsonify({'message': 'The user has been deleted!'})


@app.route('/api/login')
def login():
    auth = request.authorization
    invalid_response = {'WWW-Authenticate': 'Basic realm="Login required!"'}

    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify',
                             401,
                             invalid_response)

    user = User.query.filter_by(name=auth.username).first()

    if not user:
        return make_response('Could not verify',
                             401,
                             invalid_response)

    if check_password_hash(user.password, auth.password):
        exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
        token = jwt.encode({'public_id': user.public_id, 'exp': exp},
                           app.config['SECRET_KEY'])
        return jsonify({'token': token.decode('UTF-8')})

    return make_response('Could not verify',
                         401,
                         invalid_response)


@app.route('/api/user/change_password', methods=['POST'])
@token_required
def change_password(user):
    data = request.get_json()
    old_pasword = data['old_password']
    new_password = data['new_password']
    repeat_password = data['repeat_password']

    if new_password != repeat_password:
        return jsonify({'message': 'Passwords don\'t match'})

    if not check_password_hash(user.password, old_pasword):
        return jsonify({'message': 'Cannot perform that function!'})

    hashed_password = generate_password_hash(new_password, method='sha256')
    user.password = hashed_password
    db.session.commit()
    return jsonify({'message': 'User updated!'})
