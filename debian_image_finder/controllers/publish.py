from debian_image_finder import app
from debian_image_finder import db
from flask import request, jsonify
from debian_image_finder.models import Image, images_schema
from debian_image_finder.controllers.user import token_required
from datetime import datetime


# endpoint to publish new images from Salsa pipeline
@app.route('/api/publish/<provider_id>', methods=['POST'])
@token_required
def publish(user, provider_id):
    items = request.json['items']

    packages_str = ''
    new_images = []
    for item in items:
        if 'packages' in item['data']:
            packages = item['data']['packages']
            for package in packages:
                packages_str += ('- ' + '**' + package['name'] +
                                 '**' + ': ' + package['version'] +
                                 '\n')

        if item['kind'] == 'Upload':
            upload = item['metadata']['labels']['upload.cloud.debian.org/type']
            if upload == 'release' or upload == 'daily':
                info = item['metadata']['labels']
                arch = info['debian.org/arch']
                release = info['debian.org/release']
                img_type = info['upload.cloud.debian.org/type']
                vendor = info['cloud.debian.org/vendor']
                version = info['cloud.debian.org/version']
                ref = item['data']['ref']
                packages = packages_str
                created_at = datetime.utcnow()

                if 'aws.amazon.com/region' in info:
                    region = info['aws.amazon.com/region']
                else:
                    region = 'all'

                new_image = Image(arch,
                                  release,
                                  img_type,
                                  vendor,
                                  version,
                                  region,
                                  ref,
                                  packages,
                                  created_at,
                                  provider_id)

                db.session.add(new_image)
                db.session.commit()

                new_images.append(new_image)

    result = images_schema.dump(new_images)

    return jsonify({'images': result})
