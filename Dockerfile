FROM debian:bullseye-slim

LABEL Cloud Team "debian-cloud@lists.debian.org"

EXPOSE 5000

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    python3-dev \
    python3-flask \
    python3-flask-marshmallow \
    python3-flask-migrate \
    python3-flask-seeder \
    python3-flask-script \
    python3-flask-sqlalchemy \
    python3-flask-testing \
    python3-jwt \
    python3-markdown2 \
    python3-marshmallow-sqlalchemy \
    python3-pip \
    python3-psycopg2 \
    python3-requests \
    python3-setuptools \
    python3-bs4 \
    libjs-bootstrap4 \
    libpq-dev \
    libjs-jquery \
    libjs-jquery-datatables \
    fonts-font-awesome && \
    pip --no-cache-dir install flask-wtf && \
    apt-get -q -y clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /debian-image-finder

VOLUME /debian-image-finder

COPY . /debian-image-finder

CMD ["python3", "manage.py", "run", "-h", "0.0.0.0"]
