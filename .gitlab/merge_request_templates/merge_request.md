# Proposed Changes

Make a brief outline of your changes. Make sure you have mentioned the related issue (use a hyperlink).

# Type of Changes

What type of change this Merge Request brings to Cloud Image Finder?

- New backend feature or update.
- New frontend feature or update.
- Bug fix.
- Another change (what was the change?).

# Screenshots

If necessary, use screenshots to show us your changes on the frontend.

# Further comments

If you have something more to tell us (questions, explanations, etc) write them down here. We'll answer you as soon as possible.