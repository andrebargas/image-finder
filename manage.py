from debian_image_finder import app, db
from flask.cli import FlaskGroup
from debian_image_finder.models import User
from werkzeug.security import generate_password_hash
import sys
import uuid
import unittest
import datetime
import getpass
import jwt

cli = FlaskGroup(app)


@cli.command()
def test():
    """Runs the tests without code coverage"""
    test_path = 'debian_image_finder/tests'
    tests = unittest.TestLoader().discover(test_path, pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(result)


@cli.command('recreate_db')
def recreate_db():
    recreate_answer = None
    while recreate_answer not in ("yes", "no"):
        recreate_answer = input("Are you sure? (yes or no) ")
    if recreate_answer == "yes":
        db.drop_all()
        db.create_all()
        db.session.commit()
        print("Database recreated!")
    elif recreate_answer == "no":
        print("Aborting...")
    else:
        print("Please enter yes or no.")


@cli.command('create_user')
def create_user():
    name = input("Name: ")
    password = getpass.getpass(prompt='Password: ')

    admin_answer = None
    while admin_answer not in ("yes", "no"):
        admin_answer = input("Admin: (yes or no) ")
        if admin_answer == "yes":
            admin = True
        elif admin_answer == "no":
            admin = False
        else:
            print("Please enter yes or no.")

    hashed_password = generate_password_hash(password, method='sha256')

    new_user = User(public_id=str(uuid.uuid4()),
                    name=name,
                    password=hashed_password,
                    admin=admin)
    db.session.add(new_user)
    db.session.commit()
    print('User successfully created!')


@cli.command('create_token')
def create_token():
    public_id = input("Admin ID: ")
    years = input("Valid token time(years): ")
    days = 365 * int(years)
    exp = datetime.datetime.utcnow() + datetime.timedelta(days=days)
    token = jwt.encode({'public_id': public_id, 'exp': exp},
                       app.config['SECRET_KEY'])
    print('Token successfully created!')
    print(token.decode('UTF-8'))


if __name__ == "__main__":
    cli()
