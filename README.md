# Debian Cloud Image Finder

This app aims to make Debian official cloud images more easily accessible to users. Users will be able to search for images based on their attributes, follow instructions to run the images on providers and also follow a RSS feed which will notify about new images releases.

## Dependencies

- docker
- docker-compose

If you prefer you can set up a `virtualenv`, install the dependencies described in `requirements.txt`, and configure the Postgres database manually.

## Running the app

* Build docker image: `docker-compose build`
* Run containers: `docker-compose up`
* Setup the database: `docker exec debian-image-finder python3 manage.py db init`
* Create database migations: `docker exec debian-image-finder python3 manage.py db migrate`
* Apply database migrations: `docker exec debian-image-finder python3 manage.py db upgrade`
* Seed database: `docker exec debian-image-finder flask seed run`

> If you want to drop the current app database and create a brand new one run: `docker exec -it debian-image-finder python3 manage.py recreate_db`

* Create user: `docker exec -it debian-image-finder python3 manage.py create_user`
* Create admin token: `docker exec -it debian-image-finder python3 manage.py create_token`

## Running automated tests

* Create test database: `docker exec postgres psql -U finder -c "CREATE DATABASE finder_test;"`
* Run tests: `docker exec debian-image-finder python3 manage.py test`

## Create/Edit providers description

* Providers description are located on folder `markdown` and it's separated by files with providers name.
* If you are willing to edit any provider just edit this files and send us a MR.
* In case you want to add a new provider, first create a `.md` file with provider name and then add the full file name to `body` field on Provider model inside database.
